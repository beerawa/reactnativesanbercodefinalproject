import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Navi } from "./components";

export default function App() {
  return (
    <View style={styles.container}>
      <Navi />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "black",
    // alignItems: "center",
    // justifyContent: "center",
  },
});
