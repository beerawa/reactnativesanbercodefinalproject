import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

import { AuthContext } from "./Context";

export default function App({ navigation }) {
  const { signOut } = React.useContext(AuthContext);
  return (
    <SafeAreaView style={styles.Container}>
      <Button title="Menu" onPress={() => navigation.toggleDrawer()} />
      <Button title="Sign Out" onPress={() => signOut()} />
      <View>
        <Text style={styles.HeadLine}>Tentang Saya</Text>
        <Ionicons
          name="md-person"
          size={150}
          color="grey"
          style={{ textAlign: "center" }}
        />
        <Text
          style={{
            fontSize: 22,
            color: "#003366",
            fontWeight: "bold",
            textAlign: "center",
          }}
        >
          Ega Beerawa
        </Text>
        <Text
          style={{
            fontSize: 14,
            color: "#3EC6FF",
            fontWeight: "bold",
            textAlign: "center",
          }}
        >
          React Native Developer
        </Text>
      </View>
      <View style={styles.ContainerCardOne}>
        <Text style={styles.Label}>Portfolio</Text>
        <View
          style={{ flex: 1, flexDirection: "row", justifyContent: "center" }}
        >
          <View
            style={{
              justifyContent: "center",
              paddingLeft: 20,
              paddingRight: 20,
            }}
          >
            <AntDesign
              name="gitlab"
              size={50}
              color="#3EC6FF"
              style={{
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center",
              }}
            />
            <Text
              style={{
                fontSize: 11,
                color: "#003366",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              @egabeerawa
            </Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              paddingLeft: 20,
              paddingRight: 20,
            }}
          >
            <AntDesign name="github" size={50} color="#3EC6FF" />
            <Text
              style={{
                fontSize: 11,
                color: "#003366",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              @egabeerawa
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.ContainerCardTwo}>
        <Text style={styles.Label}>Hubungi Saya</Text>
        <View style={{ flex: 1, alignItems: "center" }}>
          <View style={{ flex: 1, flexDirection: "row", marginTop: 10 }}>
            <AntDesign name="facebook-square" size={24} color="#3EC6FF" />
            <Text
              style={{
                fontSize: 11,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 5,
                marginLeft: 7,
              }}
            >
              egabeerawa
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <AntDesign name="instagram" size={24} color="#3EC6FF" />
            <Text
              style={{
                fontSize: 11,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 5,
                marginLeft: 7,
              }}
            >
              egabeerawa
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <AntDesign name="twitter" size={24} color="#3EC6FF" />
            <Text
              style={{
                fontSize: 11,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 5,
                marginLeft: 7,
              }}
            >
              egabeerawa
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    justifyContent: "center",
  },

  HeadLine: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 36,
    color: "#003366",
    marginTop: 40,
    marginBottom: 40,
  },

  Label: {
    marginTop: 1,
    marginBottom: 1,
    marginLeft: 5,
    marginRight: 5,
    fontSize: 12,
    color: "#003366",
    borderBottomColor: "#003366",
    borderBottomWidth: 1,
  },

  ContainerCardOne: {
    // flex: 1,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 45,
    marginRight: 45,
    height: 100,
    backgroundColor: "#EFEFEF",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#fff",
    // justifyContent: "center",
  },

  ContainerCardTwo: {
    // flex: 1,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 45,
    marginRight: 45,
    height: 150,
    backgroundColor: "#EFEFEF",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#fff",
    // justifyContent: "center",
  },

  ButtonOne: {
    marginTop: 10,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 140,
    marginRight: 140,
    backgroundColor: "#003366",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },

  ButtonTwo: {
    marginTop: 10,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 140,
    marginRight: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },

  TextOne: {
    marginTop: 2,
    marginBottom: 2,
    color: "#3EC6FF",
    textAlign: "center",
    fontSize: 18,
  },

  ButtonTextStyle: {
    color: "#fff",
    textAlign: "center",
    fontSize: 18,
  },
});
