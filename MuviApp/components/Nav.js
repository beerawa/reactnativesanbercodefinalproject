import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
// import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import LoginScreen from "./LoginScreen";
import LoginScreenRegister from "./LoginScreenRegister";
import AboutScreen from "./AboutScreen";
import Movinfo from "./Movinfo";
// import SkillScreen from "../Tugas14/SkillScreen";
// import ProjectScreen from "./ProjectScreen";
// import AddScreen from "./AddScreen";

import { AuthContext } from "./Context";
import { Splash } from "./Screen";

const AuthStack = createStackNavigator();
const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
// const Tabs = createBottomTabNavigator();

const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="LoginScreen"
      component={LoginScreen}
      options={{ title: "LoginScreen" }}
    />
    <AuthStack.Screen
      name="LoginScreenRegister"
      component={LoginScreenRegister}
      options={{ title: "Create Account" }}
    />
  </AuthStack.Navigator>
);

// const TabsScreen = () => (
//   <Tabs.Navigator>
//     <Tabs.Screen name="Skill" component={SkillScreen} />
//     <Tabs.Screen name="Project" component={ProjectScreen} />
//     <Tabs.Screen name="Add" component={AddScreen} />
//   </Tabs.Navigator>
// );

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Profile">
    <Drawer.Screen name="About" component={AboutScreen} />
    <Drawer.Screen name="Movie Info" component={Movinfo} />
  </Drawer.Navigator>
);

const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen
        name="App"
        component={DrawerScreen}
        options={{
          animationEnabled: false,
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    )}
  </RootStack.Navigator>
);

export default () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUserToken("asdf");
      },
      signUp: () => {
        setIsLoading(false);
        setUserToken("asdf");
      },
      signOut: () => {
        setIsLoading(false);
        setUserToken(null);
      },
    };
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
