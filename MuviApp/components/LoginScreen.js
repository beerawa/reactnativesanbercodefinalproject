import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

import { AuthContext } from "./Context";

import LoginScreenRegister from "./LoginScreenRegister";
import AboutScreen from "./AboutScreen";

export default function App({ navigation }) {
  const { signIn } = React.useContext(AuthContext);

  return (
    <SafeAreaView style={styles.Container}>
      <Image source={require("./images/logo.jpg")} />
      <Text style={styles.HeadLine}>Login</Text>
      <View style={styles.ContainerInput}>
        <Text style={styles.Label}>Username / Email</Text>
        <TextInput
          style={{
            height: 40,
            width: 300,
            borderColor: "#003366",
            borderWidth: 1,
          }}
        />
        <Text style={styles.Label}>Password</Text>
        <TextInput
          style={{
            height: 40,
            width: 300,
            borderColor: "#003366",
            borderWidth: 1,
          }}
        />
      </View>
      <TouchableOpacity
        style={styles.ButtonTwo}
        activeOpacity={0.5}
        onPress={() => signIn()}
      >
        <Text style={styles.ButtonTextStyle}> Masuk </Text>
      </TouchableOpacity>
      {/* <Text style={styles.TextOne}>Atau</Text>
      <TouchableOpacity
        style={styles.ButtonOne}
        activeOpacity={0.5}
        onPress={() => navigation.push("LoginScreenRegister")}
      >
        <Text style={styles.ButtonTextStyle}> Daftar? </Text>
      </TouchableOpacity> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    justifyContent: "center",
  },

  HeadLine: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    color: "#003366",
    marginTop: 40,
    marginBottom: 40,
  },

  Label: {
    marginTop: 10,
    marginBottom: 2,
    fontSize: 12,
    color: "#003366",
  },

  ContainerInput: {
    // flex: 1,
    marginLeft: 45,
    marginRight: 45,
    // justifyContent: "center",
  },

  ButtonOne: {
    marginTop: 10,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 140,
    marginRight: 140,
    backgroundColor: "#003366",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },

  ButtonTwo: {
    marginTop: 10,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 140,
    marginRight: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },

  TextOne: {
    marginTop: 2,
    marginBottom: 2,
    color: "#3EC6FF",
    textAlign: "center",
    fontSize: 18,
  },

  ButtonTextStyle: {
    color: "#fff",
    textAlign: "center",
    fontSize: 18,
  },
});
